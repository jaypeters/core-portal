FROM registry.depot.io:443/salesupply-production-linux-7.2.13-zts-fpm-alpine3.8

# These are the secrets we inject
ARG DBHOST
ARG DBUSER
ARG DBPASS
ARG DBNAME
ARG DBPORT
ARG DBCHAR
ARG ERROR_REPORTING_WEBHOOK_SECRET
ARG DATA_ENCRYPT_HALITE_KEY
ARG DATA_HASH_HALITE_KEY
ARG DATA_HASH_HALITE_PEPPER
ARG SALESUPPLY_API_KEY_GOOGLE_MAPS
ARG DOCKER_ENV
ARG NGINX_SERVER_NAME
ARG NGINX_ROOT_FOLDER
ARG NGINX_SERVER_PORT

# Configure image & add application
COPY deploy /
COPY . /var/www/html/

# Replace secrets in configs
RUN cat /etc/nginx/nginx.conf | \
    sed s:##NGINX_SERVER_NAME##:"$NGINX_SERVER_NAME":g | \
    sed s:##NGINX_ROOT_FOLDER##:"$NGINX_ROOT_FOLDER":g | \
    sed s:##NGINX_SERVER_PORT##:"$NGINX_SERVER_PORT":g | \
    sed s:"root /var/www/html;":"root /var/www/html/public;":g | \
    cat - &> /etc/nginx/nginx.conf.new; \
    mv /etc/nginx/nginx.conf.new /etc/nginx/nginx.conf; \
    mv config/autoload/deploy.php.dist config/autoload/deploy.php; \
    sed -i "s/##DATA_ENCRYPT_HALITE_KEY##/$DATA_ENCRYPT_HALITE_KEY/" config/autoload/deploy.php; \
    sed -i "s/##DATA_HASH_HALITE_KEY##/$DATA_HASH_HALITE_KEY/" config/autoload/deploy.php; \
    sed -i "s/##DATA_HASH_HALITE_PEPPER##/$DATA_HASH_HALITE_PEPPER/" config/autoload/deploy.php; \
    sed -i "s/##SALESUPPLY_API_KEY_GOOGLE_MAPS##/$SALESUPPLY_API_KEY_GOOGLE_MAPS/" config/autoload/deploy.php; \
    sed -i "s/##DBNAME##/$DBNAME/" config/autoload/deploy.php; \
    sed -i "s/##DBUSER##/$DBUSER/" config/autoload/deploy.php; \
    sed -i "s/##DBPASS##/$DBPASS/" config/autoload/deploy.php; \
    sed -i "s/##DBHOST##/$DBHOST/" config/autoload/deploy.php; \
    sed -i "s/##DBPORT##/$DBPORT/" config/autoload/deploy.php; \
    sed -i "s/##DBCHAR##/$DBCHAR/" config/autoload/deploy.php; \
    cat "/development/crontabs-$DOCKER_ENV" >> /etc/crontabs/root